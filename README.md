# **Tecnológico Nacional de México**
**Instituto Tecnológico de Oaxaca**
## Programación Lógica y Funcional
SCC1019 ISB
### Act 2. Programación Declarativa y Funcional

#### _**Mapa 1: Programación Declarativa. Orientaciones y pautas para el estudio**_
Años de autoria: 1998, 1999, 2001

```plantuml
@startmindmap
title Mapa 1: Programación declarativa: Orientaciones y Pautas de estudio.
*[#Coral] Orientaciones y Pautas\nde la Prog. Declarativa
**[#Red] Programación declarativa

***_ Evolución
****[#lightblue] Codigo máquina
*****[#lightblue] Secuencia de órdenes directas
******_ y
*******[#lightblue] secuencias de muy bajo nivel
********_ pero
*********[#lightblue] muy complicado de entender
****[#lightblue] Lenguaje de Alto Nivel
*****[#lightblue] Lenguaje intermedio
******_ que es
*******[#lightblue] fácil de entender
********_ pero
*********[#lightblue] el programador
**********_ debe
***********[#lightblue] tener un control detallado
************_ sobre
*************[#lightblue] la secuencia en las que se ejecutan
**************_ las
***************[#lightblue] instrucciones
*************[#lightblue] la memoria
**************_ que
***************[#lightblue] el programa ocupa\nen todo momento

***_ Sobrenombres
****[#lightblue] Programación "perezosa"
****[#lightblue] Programación "cómoda"

***_ Función
****[#lightblue] Liberarse de tantas asignaciones
****[#lightblue] Dejar de detallar el control\n de la gestión de memoria
****[#lightblue] Usar otros recursos expresivos\npara especificar
****[#lightblue] "Facilita" la programación

***_ Ventajas
****[#lightblue] Programas más cortos
****[#lightblue] Programas más fáciles de realizar
*****[#lightblue] el tiempo de desarrollo
******_ es
*******[#lightblue] más rápido
********_ comparado con
*********[#lightblue] lenguaje convencional
****[#lightblue] Programas más fáciles de depurar
*****[#lightblue] el tiempo de modificación
******_ y
*******[#lightblue] depuración
********_ es
*********[#lightblue] mucho menor
****[#lightblue] Utilidad
*****_ porque
******[#lightblue] es más "cómodo"
*******_ de
********[#lightblue] programar
******[#lightblue] ayuda a entender mejor
*******_ las
********[#lightblue] abstracciones

***_ Ramas y cuestiones
****[#lightblue] "¿Si se le quitan asignaciones al progra-\nmador, cómo podrá programar?"
*****[#lightblue] Más que una desventaja,\nse puede volver una ventaja
*****[#lightblue] Programación declarativa
******_ se asemeja como
*******[#lightblue] arquitectura
********_ donde
*********[#lightblue] parte creativa
**********_ es
***********[#lightblue] arquitecto
************_ el cual es
*************[#lightblue] el programador
*********[#lightblue] parte "burocrática"
**********_ donde entra
***********[#lightblue] la logistica de obras y materiales
************_ el cual es
*************[#lightblue] instrucciones detalladas del programa
******_ se resumen en
*******[#lightblue] parte creativa
********_ la conforman
*********[#lightblue] la idea
**********_ aqui entra
***********[#lightblue] la función declarativa
************_ donde
*************[#lightblue] abunda la creatividad
**************_ y
***************[#lightblue] el ordenador es quien gestiona
*********[#lightblue] el algoritmo
*******[#lightblue] parte "burocrática"
********_ que incluye
*********[#lightblue] la gestión de memoria
*********[#lightblue] secuencia estricta
**********_ como ejemplo puro
***********[#lightblue] lenguaje máquina
************_ donde
*************[#lightblue] Creatividad es nula
**************_ pero
***************[#lightblue] gestión de memoria es compleja
******_ se divide en

*******[#lightblue] Paradigma lógico
********_ se basa en
*********[#lightblue] modelo
**********_ de
***********[#lightblue] lógica
***********[#lightblue] demostración automática
********[#lightblue] la forma de hacer cómputo
*********_ es que
**********[#lightblue] el intérprete opera
***********_ bajo
************[#lightblue] algoritmos de resolución
********_ lenguajes
*********[#lightblue] Prolog
**********[#lightblue] lenguaje estándar para la prog. lógica
********_ ¿cómo funciona?
*********[#lightblue] usa como expresiones
**********_ las
***********[#lightblue] axiomas
***********[#lightblue] reglas de inferencia
************_ donde
*************[#lightblue] el compilador actua
**************_ como
***************[#lightblue] motor de inferencia
*********[#lightblue] se acude a la lógica del predicado\nde primer orden
**********_ como
***********[#lightblue] relaciones entre objetos
************_ por lo tanto
*************[#lightblue] no establecen orden
**************_ entre
***************[#lightblue] argumento de entrada y\nargumento de salida
****************_ eso provoca que
*****************[#lightblue] sean más declarativos
********_ usos
*********[#lightblue] Inteligencia Artificial

*******[#lightblue] Paradigma funcional
********_ lenguajes
*********[#lightblue] Lisp
**********[#lightblue] considerado como híbrido
***********_ puede actuar cómo
************[#lightblue] funcional
************[#lightblue] imperativo
*********[#lightblue] Haskell
**********[#lightblue] lenguaje estándar para la prog. funcional
*********[#lightblue] Gofer
********_ caracteristicas
*********[#lightblue] Datos de entrada
**********_ actúan como
***********[#lightblue] argumentos
*********[#lightblue] Datos de salida
**********_ actúan como
***********[#lightblue] resultado
********_ ventajas
*********[#lightblue] uso de funciones de órden superior
**********_ una vez que
***********[#lightblue] se obtienen como objeto\principal las funciones
************_ se puede definir
*************[#lightblue] funciones que actúen sobre funciones
*********[#lightblue] "Evaluación perezosa"
**********[#lightblue] Se evalúan las ecuaciones
***********_ se calcula
************[#lightblue] hasta que alguien o algo lo requiera
************[#lightblue] solo lo necesario
*************_ esto provoca
**************[#lightblue] ventajas
***************_ como
****************[#lightblue] Poder definir números infinitos
*****************_ como
******************[#lightblue] Los números de la Serie de Fibonnaci

********_ definición
*********[#lightblue] Paradigma que recurre al\nlenguaje matemático
**********_ toma de modelo
***********[#lightblue] la forma en que los matemáticos\ndefinen funciones
************_ como
*************[#lightblue] la aplicación de una función
**************_ mediante
***************[#lightblue] reglas de reescritura
***************[#lightblue] simplificación de expresiones
**********_ consiste en que
***********[#lightblue] las tareas rutinarias\ndeprogramación
************_ se dejan al
*************[#lightblue] compilador
**************_ para que
***************[#lightblue] programador
****************_ pueda dejar
*****************[#lightblue] las tareas más rutinarias
**********_ cuando
***********[#lightblue] describen funciones
************_ usando
*************[#lightblue] razonamiento ecuacional

********_ propósito
*********[#lightblue] reducir
**********[#lightblue] la complejidad de los programas
**********[#lightblue] el riesgo
***********_ de
************[#lightblue] cometer errores
*************_ ¿haciendo qué?
**************[#lightblue] programas
*********[#lightblue] evitar
**********[#lightblue] La farragosidad del código
***********_ cuando
************[#lightblue] se escribe

***_ Definición
****[#lightblue] Abandona la secuencia de órdenes
*****_ y
******[#lightblue] le da explicaciones
*******_ del
********[#lightblue] algoritmo
*********_ que
**********[#lightblue] se representará
****[#lightblue] Estilo o paradigma
*****_ que surge como
******[#lightblue] solución a problemas
*******_ de la
********[#lightblue] programación imperativa
*********_ conocido también como
**********[#lightblue] programación clásica
***********_ con lenguajes como
************[#lightblue] C
************[#lightblue] Pascal
***********_ basado en
************[#lightblue] Modelo de Von Neumann

*******_ ¿que problemas?
********[#lightblue] El programador
*********_ está obligado a
**********[#lightblue] dar muchos detalles sobre los\ncálculos que se realizarán
***********_ en conclusión
************[#lightblue] obliga al programador
*************_ a tomar en cuenta
**************[#lightblue] más detalles de lo necesario\n para una especificación


**[#Red] Instrumento esencial de\nla programación
***_ es
****[#lightblue] la asignación de memoria
*****_ donde se modifica
******[#lightblue] el estado de la memoria\npaso por paso
*******_ esto puede provocar
********[#lightblue] cuello de botella

header
Rodriguez Martinez Joel Jafet
endheader

@endmindmap
```

#### _**Mapa 2: Lenguaje de Programación Funcional**_
Año de autoria: 2015

```plantuml
@startmindmap
title Mapa 1: Programación declarativa: Orientaciones y Pautas de estudio.
*[#Coral] Lenguaje de\nProgramación \nFuncional

**_ ¿Tienen una utilidad?
***[#Red] Permiten realizar prototipos rapidamente
***[#lightgreen] Son eficientes
***[#lightgreen] Más rapido para ser programado
****_ pero
*****[#lightgreen] requiere de un coste\ncomputacional\nmuy elevado

**_ Ejemplos
***[#Red] Realización de un Sudoku
***[#lightgreen] Juegos de Arcade
***[#lightgreen] Editor de texto
****_ tipo
*****[#lightgreen] Microsoft Word

**[#Red] X variables existen
***_ pero
****[#lightgreen] solamente para referirse
*****_ a 
******[#lightgreen] parámetros de las mismas.
*******_ si
********[#lightgreen] Una función
*********_ ¿Cómo se evaluan?
**********[#lightgreen] Evaluación perezosa
***********_ síntesis
************[#lightgreen] "Nunca trabaja de más,\npero tiene buena memoria"
***********_ tambien conocido
************[#lightgreen] evaluación impaciente
************[#lightgreen] evaluación estricta
*************_ inversa
**************[#lightgreen] evaluación no estricta
**************_ definición
***************[#lightgreen] Evaluar desde afuera hasta dentro
**************_ nacimiento
***************[#lightgreen] Donald Michie en el 62
****************_ acuña el término
*****************[#lightgreen] memoización
******************_ que es
*******************[#lightgreen] Almacenar el valor de una\nfunción cuya evaluación\nya ha sido realizada
********************_ tambien conocido
*********************[#lightgreen] Paso por necesidad
***********_ evalua 
************[#lightgreen] el interior 
*************_ y hasta que
**************[#lightgreen] no lo hace
***************[#lightgreen] no evalua lo exterior

*********_ devuelve
**********[#lightgreen] el mismo resultado
***********_ puede considerarse
************[#lightgreen] Función Constante

***_ ventajas
****[#Red] En entorno multiprocesador
*****[#lightgreen] al no depender los resultados\nde operaciones
******_ de
*******[#lightgreen] otras
********_ se pueden
*********[#lightgreen] paralelizar
**********_ por ejemplo
***********[#lightgreen] Algoritmo "divide y vencerás"

****[#Red] Al no tener variables
*****_ con
******[#lightgreen] valor determinado
*******_ los programas
********[#lightgreen] solo dependerán
*********_ de
**********[#lightgreen] parámetros de entrada
***********_ osea
************[#lightgreen] Una func. con los mismos\nparámetros de entrada
*************_ siempre devolverá
**************[#lightgreen] mismo valor
***************_ esto es conocido como
****************[#lightgreen] Transparencia Referencial
*****************_ ¿qué es?
******************[#lightgreen] Resultados son independientes\ndel orden en el que\nrealiza los cálculos
******************[#lightgreen] en imperativo
*******************_ una función\npuede hacer
********************[#lightgreen] efectos colaterales
*********************_ a la hora de
**********************[#lightgreen] devolver resultados.

**[#Red] Bases
***[#lightgreen] ¿Qué es un programa?
****_ Imperativo
*****_ usando lenguajes
******[#lightgreen] C
******[#lightgreen] Pascal
******[#lightgreen] Java
*****[#lightgreen] Una colección de datos\no serie de instrucciones
******_ que
*******[#lightgreen] operan sobre dichos datos
********_ sobre
*********[#lightgreen] un orden adecuado
**********_ se podrán modificar 
***********[#lightgreen] según el estado de variables
***********[#lightgreen] bajo la ejecución de ese orden

****_ Funcional
*****[#lightgreen] Consecuencias
******[#lightgreen] Como se trabaja
*******_ bajo
********[#lightgreen] funciones matemáticas
*********_ cambia
**********[#lightgreen] el Concepcto de Asignación
***********_ ejemplos
************[#lightgreen] en imperativo
*************[#lightgreen] Si x=x+2
**************[#lightgreen] El valor de x se modificará\nincrementando 2 unidades
************[#lightgreen] en funcional
*************[#lightgreen] Si x=x+2
**************[#lightgreen] x será igual a su mismo\nvalor mas dos
***************_ lo cual
****************[#lightgreen] no existe
***********_ desemboca en
************[#lightgreen] "desaparición" de bucles
*************_ pero
**************[#lightgreen] se soluciona
***************_ mediante
****************[#lightgreen] recursión
*****************_ ejemplo
******************[#lightgreen] Factorial

*****[#lightgreen] Uso de funciones 
******_ que
*******[#lightgreen] reciben parámetros de entrada
********_ y
*********[#lightgreen] paramétros de salida

**[#Red] ¿Qué son los paradigmas\nde programación?
***[#lightgreen] Modelos de computación
****_ en el que
*****[#lightgreen] Diferentes lenguajes
******_ basados
*******[#lightgreen] la mayoria
********_ en
*********[#lightgreen] Modelo de Von Neumann
**********_ evolución
***********[#lightgreen] Lógica combinatoria
************_ desarrollado por
*************[#lightgreen] Haskell Brooks Curry
**************_ fueron cimientos para
***************[#lightgreen] Peter Landin en el 65
****************_ donde descubrió
*****************[#lightgreen] que todo eso puede ser\nusado para un leng. de programación
******************_ así nace
*******************[#lightgreen] Programación Funcional

**********_ se denominan

***********[#lightgreen] Lenguajes Imperativos
************_ definición
*************[#lightgreen] Las instrucciones que se dan
**************_ son
***************[#lightgreen] órdenes
************_ variantes

*************[#lightgreen] Programación Orientada\a Objetos
**************_ definición
***************[#lightgreen] Pequeños trozos
****************_ de
*****************[#lightgreen] código
******************_ que
*******************[#lightgreen] interactuan entre sí
********************_ pero
*********************[#lightgreen] realmente se componen de instrucciones
**********************_ que
***********************[#lightgreen] se ejecutan secuencialmente

*************[#lightgreen] Lógica simbólica
**************_ es
**************[#lightgreen] un paradigma lógico
**************_ consiste en
***************[#lightgreen] un programa
****************_ con
*****************[#lightgreen] un conjunto de sentencias
******************_ que
*******************[#lightgreen] definen
********************_ mediante
*********************[#lightgreen] la aplicación
**********************_ de 
***********************[#lightgreen] la inferencia lógica
************************_ se podrá
*************************[#lightgreen] resolver problemas

********************_ aquello
*********************[#lightgreen] lo que es verdad
*********************[#lightgreen] lo que es conocido

*************[#lightgreen] "Lamda cálculo"
**************_ caracteristicas
***************[#lightgreen] Una función con n parámetros
****************_ puede ser vista como
*****************[#lightgreen] una función con\nun solo parámetro
******************_ y devuelve
*******************[#lightgreen] una función con n-1\nparámetros de salida
********************_ conocido como
*********************[#lightgreen] currificación
**********************_ en honor a
***********************[#lightgreen] Haskell Curry
**********************_ usando tambien
***********************[#lightgreen] Aplicación Parcial

**************_ desarrollado
***************[#lightgreen] en los 30's por Church y Kleene
**************_ pensado primero como
***************[#lightgreen] Un sistema
****************_ que resultó
*****************[#lightgreen] un sistema computacional potente
******************_ similar al
*******************[#lightgreen] Modelo de Von Neumann
********************_ máxima
*********************[#lightgreen] "Todo lo que se puede\nhacer en uno, tambien se puede\nhacer en el otro"

****************_ para
*****************[#lightgreen] estudiar
******************[#lightgreen] El concepto de función
******************[#lightgreen] Recursividad
******************[#lightgreen] Aplicación de funciones

**********_ definiciones
***********[#lightgreen] Todo gira alrededor
************_ de
*************[#lightgreen] las funciones
**************_ pueden ir
***************[#lightgreen] en cualquier parte
****************_ incluso
*****************[#lightgreen] como parametro
******************_ de
*******************[#lightgreen] otras funciones
********************_ osea
*********************[#lightgreen] ubicúas
**********************_ ejemplo
***********************[#lightgreen] Func. de orden\nsuperior
**************_ conocido como
***************[#lightgreen] "Ciudadanos de\nPrimera Clase"

***********[#lightgreen] Ejecución
************[#lightgreen] Serie de instrucciones
*************_ que 
**************[#lightgreen] se irán ejecutandose secuencialmente
***********[#lightgreen] Secuencias
************[#lightgreen] Se podrán modificar
*************_ según
**************[#lightgreen] estado del cómputo
***********[#lightgreen] Estado de Cómputo
************[#lightgreen] Zona de memoria
*************_ que
**************[#lightgreen] se podrá acceder
***************_ mediante
****************[#lightgreen] una serie
*****************_ de
******************[#lightgreen] variables
*******************_ podrán
********************[#lightgreen] ser modificadas
*********************_ mediante

**********************[#lightgreen] Instrucción de Asignación
***********************_ ¿en qué consiste?
************************[#lightgreen] A una determinada variable
*************************_ se asigna
**************************[#lightgreen] un valor
***************************_ y
****************************[#lightgreen] hasta que dicho valor no se cambie
*****************************_ tendrá
******************************[#lightgreen] el mismo valor

*******************_ que en un momento dado
********************[#lightgreen] almacenará todo el valor
*********************_ de
**********************[#lightgreen] todas las variables definidas

**********_ propone que
***********[#lightgreen] los programas deben almacenarse
************_ en 
*************[#lightgreen] la misma máquina
**************_ antes de realizar
***************[#lightgreen] la ejecución


******_ datan de
*******[#lightgreen] semántica
********_ a
*********[#lightgreen] los programas

header
Rodriguez Martinez Joel Jafet
endheader

@endmindmap
```

